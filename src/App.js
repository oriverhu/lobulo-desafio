import React from 'react';
import Table from './components/Table';
import { Container,Header } from 'semantic-ui-react'

function App() {
  return (
    <Container style={{ margin: 50, backgroundColor: '#ccc4c4' }}>
      <Header as="h1" style={{ textAlign: 'center'}}>Lobulo Media</Header>
      <Table />
   </Container>
  );
}

export default App;
