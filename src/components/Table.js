import React from 'react'
import { Container, Table, Input, Select } from 'semantic-ui-react'
import datos from './../json/data.json';

const Options = [
    { key: '1', value: 'Mobile', text: 'Mobile' },
    { key: '2', value: 'Desktop', text: 'Desktop' },
  ]

export default class Data extends React.Component {

    state = {
        data: datos
    }

    handleChange = ({target: {value}}) => {
        if(value!=""){
            var arr = [];
        
            let results = datos.map((item, index) => {
                if(value==item.id){
                    arr.push(item);
                }
            });

            if(arr.length>0){
                this.setState({
                    data:arr
                })
            }
        }else{
            this.setState({
                data:datos
            })
        }
    }

    handleSelect = (e, data) => {
     var value =data.value;
        if(value!=""){
            var arr = [];
        
            let results = datos.map((item, index) => {
                if(value==item.type){
                    arr.push(item);
                }
            });

            if(arr.length>0){
                this.setState({
                    data:arr
                })
            }
        }else{
            this.setState({
                data:datos
            })
        }
    }

    // componentDidMount(){
    //     let results = datos.map((item, index) => {
    //         console.log(item);
    //      });
    // }

    render(){
        const {data}= this.state;
        return(
            <Container>
                <Input onChange={this.handleChange} placeholder='Buscar Id...' />
                <Select onChange={this.handleSelect} placeholder='Seleccione...' options={Options} />
                <Table inverted>
                    <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Id</Table.HeaderCell>
                        <Table.HeaderCell>Name</Table.HeaderCell>
                        <Table.HeaderCell>Type</Table.HeaderCell>
                        <Table.HeaderCell>Size</Table.HeaderCell>
                    </Table.Row>
                    </Table.Header>

                    <Table.Body>

                    {
                    data && data.map((item,index)=>
                        <Table.Row key={index}>
                            <Table.Cell>{item.id}</Table.Cell>
                            <Table.Cell>{item.name}</Table.Cell>
                            <Table.Cell>{item.type}</Table.Cell>
                            <Table.Cell>{item.size}</Table.Cell>
                        </Table.Row>
                    )
                    }

                    </Table.Body>

                    <Table.Footer>
                    <Table.Row>
                        <Table.HeaderCell>Id</Table.HeaderCell>
                        <Table.HeaderCell>Name</Table.HeaderCell>
                        <Table.HeaderCell>Type</Table.HeaderCell>
                        <Table.HeaderCell>Size</Table.HeaderCell>
                    </Table.Row>
                    </Table.Footer>
                </Table>
            </Container>
        )
    }
}

